public class Grading{
	public void grading(int marks) {
		if ((marks <= 100) && (marks >= 75)) {
			System.out.println("A");
		}
		if ((marks <= 74) && (marks >= 65)) {
			System.out.println("B");
		}
		if ((marks <= 64) && (marks >= 55)) {
			System.out.println("C");
		}
		if ((marks <= 54) && (marks >= 35)) {
			System.out.println("D");
		}
		if (marks <= 34) {
			System.out.println("F");
		}
	}
}
