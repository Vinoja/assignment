import java.util.ArrayList;
import java.util.Scanner;

public class StudentDetails {
	private String studentID;
	private String studentName;
	private int grade;
	private int Acc;
	private int Bus;
	private int Eco;
	private int total;
	private float percent;

	public StudentDetails(String studentID, String studentName, int grade, int Acc, int Bus, int Eco) {
		this.studentID = studentID;
		this.studentName = studentName;
		this.grade = grade;
		this.Acc = Acc;
		this.Bus = Bus;
		this.Eco = Eco;
	}

	public StudentDetails() {

	}

	public String getStudentID() {
		return studentID;
	}

	public String getStudentName() {
		return studentName;
	}

	public int getStudentGrade() {
		return grade;
	}

	public int Acc() {
		return Acc;
	}

	public int Bus() {
		return Bus;
	}

	public int Eco() {
		return Eco;
	}

	public void selectCall() {
		System.out.println("");
		System.out.println("\tSTUDENT DETAILS HERE");
		System.out.println("");
		System.out.println("STUDENT DETAILS,ALL STUDENT DETAILS,HIGHER RANK HOLDER DETAILS");
		System.out.println("");
		StudentDetails select = new StudentDetails();
		Scanner input = new Scanner(System.in);
		System.out.print("TYPE\t: ");
		int type = input.nextInt();
		System.out.println();
		select.main(type);
	}

	public void main(int type) {
		ArrayList<StudentDetails> studentList = new ArrayList<>();
		StudentDetails Kabilan = new StudentDetails("STUDENT101", "V.Kabilan", 01, 90, 80, 88);
		StudentDetails Nivethikan = new StudentDetails("STUDENT102", "S.Nivethikan", 01, 80, 55, 99);
		StudentDetails Sambavi = new StudentDetails("STUDENT103", "V.Sambavi", 01, 80, 78, 68);
		StudentDetails Jejanthan = new StudentDetails("STUDENT104", "K.Jejanthan", 01, 78, 88, 65);
		StudentDetails Keerthika = new StudentDetails("STUDENT105", "V.Keerthika", 01, 92, 84, 66);
		studentList.add(Kabilan);
		studentList.add(Nivethikan);
		studentList.add(Sambavi);
		studentList.add(Jejanthan);
		studentList.add(Keerthika);
		if (type == 1) {
			System.out.println();
			System.out.println("\tSPECIFIC STUDENT DETAILS");
			System.out.println();
			System.out.print("ENTER THE SPECIFIC STUDENT ID : ");
			Scanner input = new Scanner(System.in);
			String id = input.nextLine();
			System.out.println();
			System.out.println("\tJ/R.C.T.M.S.School");
			konsikan.Details(id);
		}
		if (type == 2) {
			System.out.println();
			System.out.println("\t J/R.C.T.M.S.School");
			konsikan.Details("STUDENT101");
			silaxan.Details("STUDENT102");
			mathiyalagan.Details("STUDENT103");
			sajanthan.Details("STUDENT104");
			aravinthan.Details("STUDENT105");
		}
		if (type == 3) {
			System.out.println();
			System.out.println(" 1-Acc 2-Bus 3-Eco");
			System.out.println();
			Scanner input = new Scanner(System.in);
			System.out.print("TYPE\t: ");
			int typeForSubject = input.nextInt();
			Main demo = new Main();
			System.out.println();
			studentList.get(1).getStudentID();
			StudentDetails topStudent = null;
			if (type == 1) {
				int maxMark = 0;
				for (int i = 0; i < studentList.size(); i++) {
					if (studentList.get(i).maths() > maxMark) {
						topStudent = studentList.get(i);
						maxMark = studentList.get(i).maths();
					}
				}
				if (topStudent != null) {
					System.out.println("TOP RANKED STUDENT DETAILS IN Acc");
					System.out.println("-----------------------------------");
					System.out.println("STUDENT ID \t: " + topStudent.getStudentID());
					System.out.println("NAME \t\t: " + topStudent.getStudentName());
					System.out.println("Age \t\t: " + topStudent.getStudentGrade());
					System.out.println("Marks \t\t: " + topStudent.Acc());
				}
			}
			if (type == 2) {
				int maxMark = 0;
				for (int i = 0; i < studentList.size(); i++) {
					if (studentList.get(i).Bus() > maxMark) {
						topStudent = studentList.get(i);
						maxMark = studentList.get(i).Bus();
					}
				}
				if (topStudent != null) {
					System.out.println("TOP RANKED STUDENT DETAILS IN Bus");
					System.out.println("-------------------------------------");
					System.out.println("STUDENT ID \t: " + topStudent.getStudentID());
					System.out.println("NAME \t\t: " + topStudent.getStudentName());
					System.out.println("Age \t\t: " + topStudent.getStudentGrade());
					System.out.println("Marks \t\t: " + topStudent.Bus());
				}
			}
			if (type == 3) {
				int maxMark = 0;
				for (int i = 0; i < studentList.size(); i++) {
					if (studentList.get(i).chemistry() > maxMark) {
						topStudent = studentList.get(i);
						maxMark = studentList.get(i).Eco();
					}
				}
				if (topStudent != null) {
					System.out.println("TOP RANKED STUDENT DETAILS IN Eco");
					System.out.println("---------------------------------------");
					System.out.println("STUDENT ID \t: " + topStudent.getStudentID());
					System.out.println("NAME \t\t: " + topStudent.getStudentName());
					System.out.println("GRADE \t\t: " + topStudent.getStudentGrade());
					System.out.println("MARKS \t\t: " + topStudent.Eco());
				}
			}

		}
	}

	public void Details(String studentID) {
		total = Acc + Bus + Eco;
		percent = total / 3;
		System.out.println("");
		System.out.println("####################################################");
		System.out.println("");
		System.out.println("STUDENT ID :" + studentID + "\t    STUDENT NAME :" + studentName);
		System.out.println("----------------------------------------------------");
		System.out.println();
		System.out.println("SUBJECT             MARKS \tGRADE");
		System.out.println();
		System.out.println("Acc                " + Acc);
		Grading grad = new Grading();
		grad.grading(maths);
		System.out.println("Bus              " + Bus);
		grad.grading(physics);
		System.out.println("Eco            " + Eco);
		grad.grading(chemistry);
		System.out.println("----------------------------------------------------");
		System.out.println("TOTAL MARKS \t\t: " + total + "/300");
		System.out.println("PERCENTAGE \t\t: " + percent);
		System.out.println("OVERALL GRADE \t\t: ");
		Grading gradPercent = new Grading();
		gradPercent.grading((int) percent);
		System.out.println();
		System.out.println("####################################################");
		System.out.println();
		System.out.println();

	}
}
